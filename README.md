# Linux4space-libcsp Demo
This repository is an example of a simple CPS server/client implementation.

## Dependencies
To build this example, you should install libcsp 1.6.

## How to operate  
Client and client_posix belong together when compiled.  
You need to use USB dongle to test out smart.  

To build client and server use this command:

```
make all
```

-a is the address of client/server, -r is the address being sent to.  
-t is test mode, -m is message mode  

```
./csp_server -k/dev/ttyUSB0 -a1  
./csp_client -k/dev/ttyUSB0 -a2 -r1
```