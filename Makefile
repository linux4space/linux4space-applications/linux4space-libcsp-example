CC=gcc
CFLAGS=
LDFLAGS=

all: csp_client csp_server

csp_client: src/csp_client.c src/csp_client_posix.c
	${CC}  src/csp_client.c src/csp_client_posix.c ${CFLAGS} ${LDFLAGS} -o csp_client -lcsp

csp_server: src/csp_server.c src/csp_server_posix.c
	${CC}  src/csp_server.c src/csp_server_posix.c ${CFLAGS} ${LDFLAGS} -o csp_server -lcsp

clean:
	rm -f csp_client csp_server